
// FaceRecognitionAppDlg.h : 头文件
//

#pragma once
#include "afxwin.h"

#include <vector>
#include <string>
#include <opencv.hpp>

#include "FaceDetect.h"
#include "MouseRespondLayer.h"
#include "BottomLayer.h"

using namespace cv;

// CFaceRecognitionAppDlg 对话框
class CFaceRecognitionAppDlg : public CDialogEx
{
// 构造
public:
	CFaceRecognitionAppDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_FACERECOGNITIONAPP_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	CRect GetMasterFramePoint();

public:
	CRect masterframerect_;
	CMouseRespondLayer mouserespondlayer_;
	BottomLayer	bottomlayer_;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

extern HWND ghwnd_idmasterframe;
