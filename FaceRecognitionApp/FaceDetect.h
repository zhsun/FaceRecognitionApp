#ifndef _FACEDETECT_H_
#define _FACEDETECT_H_

#include "config.h"
#include "facedetect-dll.h"
#pragma comment(lib,"libfacedetect.lib")

using namespace cv;
using namespace std;

//define the buffer size. Do not change the size!
#define DETECT_BUFFER_SIZE		0xC004
#define DISTANCE_SENSITIVITY	10

typedef struct _deta_{
	int		sn;
	float	deta;
}DETA_;


extern Ptr<FaceRecognizer> faces_model;


void Dlib_Predefine();//dlib Ԥ����ĺ���
Mat FaceAlignment(Mat frame, Rect point);
std::vector<Rect> detectFaces(Mat frame);
std::vector<Rect> trackFaces(std::vector<Rect> newpoints);
void quickSort(float deta[], int start, int datanumber, int newpointsn[], int lastpointsn[]);
void quickSort_ex(int lastpointsn[], int start, int datanumber, int newpointsn[], float deta[]);
inline void boxFacesAndMark(Mat frame, Rect point, int sn, int predicted = -1); //serialnumber = sn ���

#endif // _FACEDETECT_H_