//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FaceRecognitionApp.rc
//
#define ID_MENU_ICON_ANIMATION_EFFECTS  1
#define ID_VOICE_TIMER                  2
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FACERECOGNITIONAPP_DIALOG   102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define ID_MOUSERESPONDLAYER_DIALOG     132
#define ID_BOTTOMLAYER_DIALOG           136
#define IDC_BUTTON1                     1000
#define ID_MASTER_FRAME                 1002
#define IDC_BUTTON2                     1003
#define IDC_BUTTON3                     1004
#define ID_NAME                         1005
#define IDC_BUTTON4                     1006
#define IDC_BUTTON5                     1007
#define IDC_BUTTON6                     1008
#define ID_TEXT_MASTER_CAM_ID           1009
#define ID_NAME_EDIT                    1013
#define ID_TEXT_NAME                    1014
#define ID_PIC_FACE1                    1015
#define ID_PIC_FACE2                    1016
#define ID_PIC_FACE0                    1017
#define ID_PIC_FACE3                    1018
#define ID_PIC_FACE4                    1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1020
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
