
// FaceRecognitionAppDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "FaceRecognitionApp.h"
#include "FaceRecognitionAppDlg.h"
#include "afxdialogex.h"

//#include "FaceDetect.h"
//#include "FaceRotate.h"
//#include "ReadCSV.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


HWND ghwnd_idmasterframe;

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框
class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CFaceRecognitionAppDlg 对话框
CFaceRecognitionAppDlg::CFaceRecognitionAppDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFaceRecognitionAppDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFaceRecognitionAppDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CFaceRecognitionAppDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_LBUTTONDOWN()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CFaceRecognitionAppDlg 消息处理程序

BOOL CFaceRecognitionAppDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
#if _DEBUG
	InitConsoleWindow(); // 让MFC支持printf调试
#endif

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// 初始化全局变量
	memset(g_predicted, -1, sizeof(g_predicted));

	// 读取config中的配置文件
	TCHAR szPath[MAX_PATH];
	GetModuleFileName(NULL, szPath, MAX_PATH);
	g_currentpath.Format("%s", szPath);
	g_currentpath = g_currentpath.Left(g_currentpath.ReverseFind(_T('\\')) + 1);
	CString configfilepath = g_currentpath + "config\\";

	int cameraid = 0;
	CStdioFile configfile;
	CString configreadoutstr;
	CString configcommand;
	configfile.Open(configfilepath + "config.ini", CFile::modeRead | CFile::typeText);
	while (configfile.ReadString(configreadoutstr)){
		configcommand = "";
		configreadoutstr.Replace(" ", "");
		configreadoutstr.Replace("	", "");
		if (configreadoutstr.Find("//") >= 0)
			configreadoutstr = configreadoutstr.Left(configreadoutstr.Find("//"));
		if (configreadoutstr.Find("=") >= 0)
		{
			configcommand = configreadoutstr.Left(configreadoutstr.Find("="));
			configreadoutstr = configreadoutstr.Mid(configreadoutstr.Find("=") + 1);
			//configcommand.MakeUpper(); // 将CString中的字母转换为大写
		}
		if (configcommand == "cameraid"){
			cameraid = _ttoi(configreadoutstr);
		}
		if (configcommand == "master_win_width"){
			g_masterwinwidth = _ttoi(configreadoutstr);
		}
		if (configcommand == "master_win_height"){
			g_masterwinheight = _ttoi(configreadoutstr);
		}
		if (configcommand == "threshold"){
			g_threshold = _ttoi(configreadoutstr) * 1.0;

			if (g_threshold == 0)
				g_threshold = DBL_MAX;
		}
	}
	configfile.Close();

	// 判断face文件夹是否存在，若不存在则创建。
	g_faceslibpath = g_currentpath + "faceslib\\";
	if (!PathIsDirectory(g_faceslibpath)){
		if (!CreateDirectory(g_faceslibpath, NULL)){
			CString strMsg;
			strMsg.Format("创建路径\"%s\"失败！", g_faceslibpath);
			MessageBox(_T(strMsg));
			return FALSE;
		}
	}

	// 创建opencv face训练库
	CString trainlibfile = g_faceslibpath + FACE_TRAIN_LIB;
	faces_model = createEigenFaceRecognizer(80, g_threshold);//创建人脸识别类 可修改 LBPHFace、EigenFace、FisherFace
	//if (PathFileExists(trainlibfile))
	//	faces_model->load(trainlibfile.GetBuffer());
	
	// 读取训练库文件
	CStdioFile trainfile;
	CString trainlibreadoutstr;
	string facefile;
	if (trainfile.Open(g_faceslibpath + FACE_LABELS_INI, CFile::modeRead | CFile::typeText)){
		while (trainfile.ReadString(trainlibreadoutstr)){
			trainlibreadoutstr.Replace(" ", "");
			trainlibreadoutstr.Replace("	", "");
			if (trainlibreadoutstr.Find("//") >= 0)
				trainlibreadoutstr = trainlibreadoutstr.Left(trainlibreadoutstr.Find("//"));
			if (trainlibreadoutstr.Find(";") >= 0){
				// 取出face图片的地址
				facefile = trainlibreadoutstr.Left(trainlibreadoutstr.Find(";"));
				
				// 后移至下一个
				trainlibreadoutstr = trainlibreadoutstr.Mid(trainlibreadoutstr.Find(";") + 1);
				// 取出label号
				g_labels = _ttoi(trainlibreadoutstr.Left(trainlibreadoutstr.Find(";")));
				
				// 后移至下一个
				trainlibreadoutstr = trainlibreadoutstr.Mid(trainlibreadoutstr.Find(";") + 1);
				// 取出对应的姓名
				g_peoplenames[g_labels] = CT2A(trainlibreadoutstr);

				g_facesimagemat.push_back(imread(facefile, CV_LOAD_IMAGE_GRAYSCALE));
				g_faceslabels.push_back(g_labels);
			}
		}
		trainfile.Close();

		faces_model->train(g_facesimagemat, g_faceslabels);
		g_labels++;
		g_trainedflag = true;
	}

	// 初始化dlib配置
	Dlib_Predefine();

	// TODO:  在此添加额外的初始化代码
	// 获取控件句柄
	CWnd   *pWnd = GetDlgItem(ID_MASTER_FRAME);
	ghwnd_idmasterframe = pWnd->GetSafeHwnd();
	theApp.ptheappdlgdc_ = GetDC();

	// 固定住主显示框的位置
	CRect masterframerect;
	GetDlgItem(ID_MASTER_FRAME)->GetWindowRect(&masterframerect); //ID_MASTER_FRAME为Picture Control的ID  
	ScreenToClient(&masterframerect);
	GetDlgItem(ID_MASTER_FRAME)->MoveWindow(masterframerect.left, masterframerect.top, g_masterwinwidth, g_masterwinheight, true); //固定Picture Control控件的大小
	GetDlgItem(ID_MASTER_FRAME)->ShowWindow(TRUE);

	// 把显示帧的窗口关联上主显示框
	namedWindow(MASTER_WIN_NAME, WINDOW_AUTOSIZE);
	resizeWindow(MASTER_WIN_NAME, g_masterwinwidth, g_masterwinheight);
	HWND hWnd    = (HWND)cvGetWindowHandle(MASTER_WIN_NAME);
	HWND hParent = ::GetParent(hWnd);
	::SetParent(hWnd, GetDlgItem(ID_MASTER_FRAME)->m_hWnd);
	::ShowWindow(hParent, SW_HIDE);

	// 更改窗口主体的位置和大小。
	MoveWindow(0, 0, g_masterwinwidth, g_masterwinheight, TRUE);

	// 更改按钮图标
	//Button1.SetIcon(AfxGetApp()->LoadIcon(ID_TAKE_PHOTO));// HICON m_IconBtn = AfxGetApp()->LoadIcon(ID_TAKE_PHOTO); //导入Icon资源，利用m_hIconBtn来存储句柄。

	// 启动发音控制定时器
	SetTimer(ID_VOICE_TIMER, 6000, NULL);

	g_gettingcameraflag = TRUE;
	AfxBeginThread(CFaceRecognitionAppApp::OpenAndGetCameraThreadProc, (LPVOID)cameraid, THREAD_PRIORITY_NORMAL, 0, 0, NULL);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CFaceRecognitionAppDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CFaceRecognitionAppDlg::OnPaint()
{
	// 获得屏幕大小
	int screenwidth = GetSystemMetrics(SM_CXSCREEN); //屏幕宽度 
	int screenheight = GetSystemMetrics(SM_CYSCREEN); //屏幕高度 

	// 获得主显示窗口的绝对坐标
	CRect masterframerect;
	GetDlgItem(ID_MASTER_FRAME)->GetWindowRect(&masterframerect); //ID_MASTER_FRAME为Picture Control的ID 
	
	// 获得主显示窗口的相对坐标
	//ScreenToClient(&masterframerect);
	
	//获得窗体在屏幕上的位置
	//CRect masterwindowrect;
	//GetWindowRect(masterwindowrect); 
	
	// 创建底层，默认介于主显示窗口与鼠标层之间
	bottomlayer_.Create(ID_BOTTOMLAYER_DIALOG, this);
	if (bottomlayer_.m_hWnd > 0)
		bottomlayer_.MoveWindow(masterframerect.left, masterframerect.top, masterframerect.Width(), masterframerect.Height(), TRUE);
	bottomlayer_.ShowWindow(TRUE);
	ghwnd_bottomlayer = bottomlayer_.m_hWnd;

	// 创建鼠标层，并覆盖于整个桌面之上
	mouserespondlayer_.Create(ID_MOUSERESPONDLAYER_DIALOG, this);
	if (mouserespondlayer_.m_hWnd > 0)
		mouserespondlayer_.MoveWindow(0, 0, screenwidth, screenheight, TRUE);
	mouserespondlayer_.ShowWindow(TRUE);
	// 获取该层句柄
	ghwnd_mouserespondlayer = mouserespondlayer_.m_hWnd;
	//mouserespondlayer_.DestroyWindow(); // 销毁鼠标层

	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CFaceRecognitionAppDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CFaceRecognitionAppDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == ID_VOICE_TIMER){
		if (g_speakerflag)
			g_speakerflag = false;
	}

	CDialogEx::OnTimer(nIDEvent);
}
