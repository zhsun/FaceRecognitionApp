﻿
// FaceRecognitionApp.cpp : 定义应用程序的类行为。
//

#include <algorithm>

#include "stdafx.h"
#include "FaceRecognitionApp.h"
#include "FaceRecognitionAppDlg.h"
#include "tlhelp32.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CFaceRecognitionAppApp

BEGIN_MESSAGE_MAP(CFaceRecognitionAppApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CFaceRecognitionAppApp 构造

CFaceRecognitionAppApp::CFaceRecognitionAppApp()
{
	// 支持重新启动管理器
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO:  在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CFaceRecognitionAppApp 对象

CFaceRecognitionAppApp theApp;


// CFaceRecognitionAppApp 初始化

BOOL CFaceRecognitionAppApp::InitInstance()
{
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。  否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	/*用于检测所有的进程中是否已经存在本程序，以防止多次打开本程序*/
	BOOL theappruningflag = FALSE;
	CString str;
	PROCESSENTRY32 pe32;
	pe32.dwSize = sizeof(pe32);

	// 获得系统内所有进程快照
	HANDLE hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
		return FALSE;

	// 枚举列表中的第一个进程
	BOOL bProcess = Process32First(hProcessSnap, &pe32);
	while (bProcess){
		str = pe32.szExeFile;
		//比较进程名，找到要找的进程名
		if (str == "FaceRecognitionApp.exe"){
			if (theappruningflag){
				MessageBoxA(NULL, "检测到程序已经运行", "", MB_ICONWARNING);
				return FALSE;
			}
			theappruningflag = TRUE;
		}
		bProcess = Process32Next(hProcessSnap, &pe32);
	}

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// 创建 shell 管理器，以防对话框包含
	// 任何 shell 树视图控件或 shell 列表视图控件。
	CShellManager *pShellManager = new CShellManager;

	// 激活“Windows Native”视觉管理器，以便在 MFC 控件中启用主题
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO:  应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));

	CFaceRecognitionAppDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO:  在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO:  在此放置处理何时用
		//  “取消”来关闭对话框的代码

		g_gettingcameraflag = false; // 结束持续捕获摄像头的线程

		while (!g_getcameraoverflag){
			Sleep(1);
		}

	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "警告: 对话框创建失败，应用程序将意外终止。\n");
		TRACE(traceAppMsg, 0, "警告: 如果您在对话框上使用 MFC 控件，则无法 #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS。\n");
	}

	// 删除上面创建的 shell 管理器。
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

#if 1
UINT CFaceRecognitionAppApp::OpenAndGetCameraThreadProc(LPVOID lpParameter)
{
	Mat masterframecopy;
	int cameraid = (int)lpParameter;
	VideoCapture camera(cameraid);
	if (!camera.isOpened()){
		MessageBoxA(NULL, "无法打开摄像头, 请检测系统中是否有摄像头接入,并配置了正确的CameraID", "", MB_ICONWARNING);
		return 0;
	}

	while (g_gettingcameraflag){
		camera >> g_masterframe;
		resize(g_masterframe, masterframecopy, Size(g_masterwinwidth, g_masterwinheight));

		g_fristdetectpoint = detectFaces(masterframecopy);
		
		if (g_gettingcameraflag){
			imshow(MASTER_WIN_NAME, masterframecopy);

			waitKey(1);
			/*if (waitKey(1) == 27){
			break;
			}*/
		}
	}
	camera.release();

	g_getcameraoverflag = true;

	return 1;
}
#else
// 接收hi3536发送的回显图片（解码后图片）然后转存为JPG至本地，未解决Opencv无法imread图片的问题，搁置
UINT8 bk_pic_buf[2][1000000];
UINT CFaceRecognitionAppApp::OpenAndGetCameraThreadProc(LPVOID lpParameter)
{
	// UDP Server
	WSADATA  wsaData;                                   //指向WinSocket信息结构的指针
	WORD     wVersion = MAKEWORD(2, 2);
	if (WSAStartup(wVersion, &wsaData) != 0)
		return 0;

	//回显总开关发送Socket
	SOCKET sclient = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sclient == INVALID_SOCKET){
		printf("invalid socket !");
		return 0;
	}

	sockaddr_in serAddr;
	serAddr.sin_family = AF_INET;
	serAddr.sin_port = htons(41234);
	serAddr.sin_addr.S_un.S_addr = inet_addr("128.0.111.82");

	// 回显图片回送开关
	SOCKET listener;
	SOCKADDR_IN sin_recv;
	listener = socket(AF_INET, SOCK_DGRAM, 0);
	sin_recv.sin_family = AF_INET;
	sin_recv.sin_port = htons(41235);
	//sin_recv.sin_addr.s_addr = htonl(INADDR_ANY);
	sin_recv.sin_addr.S_un.S_addr = inet_addr("128.0.111.10");
	bind(listener, (SOCKADDR FAR *)&sin_recv, sizeof(sin_recv));
	int iMode = 0; //1,非阻塞；0,阻塞
	ioctlsocket(listener, FIONBIO, (u_long FAR*) &iMode);

	int timeout = 100;
	setsockopt(listener, SOL_SOCKET, SO_SNDTIMEO, (CHAR*)&timeout, sizeof(timeout));
	setsockopt(listener, SOL_SOCKET, SO_RCVTIMEO, (CHAR*)&timeout, sizeof(timeout));

	// 回显索要总开关发送
	char * sendData = "SNAP:1,128.0.111.10,41235,1,0,0,0,1920,1080";
	sendto(sclient, sendData, strlen(sendData), 0, (sockaddr*)&serAddr, sizeof(serAddr));

	byte p_num, p_sn;
	UINT8 flg_out_node_p_num;
	
	SOCKADDR_IN temp_sin;
	int nSize, nbSize;
	int pic_len;
	DWORD mask_ip = inet_addr("128.0.111.10");
	//全局内存
	HGLOBAL hGlobal;
	IStream* pStream;
	CImage image, preview_image;

	Sleep(500);

	while (1){
		p_num = 100;
		flg_out_node_p_num = 0;
		pic_len = 0;

		sin_recv.sin_addr.S_un.S_addr = inet_addr("128.0.111.82");
		sendto(listener, "SNAP:2", 6, 0, (sockaddr*)&sin_recv, sizeof(sin_recv));
		nSize = sizeof(sin_recv);

		while (flg_out_node_p_num != p_num)
		{
			nbSize = recvfrom(listener, (CHAR*)&bk_pic_buf[1][0], 60006, 0, (sockaddr*)&temp_sin, &nSize);

			//如果有图片
			if (nbSize >= 6 && temp_sin.sin_addr.S_un.S_addr != mask_ip)
			{
				p_num = bk_pic_buf[1][4];
				p_sn = bk_pic_buf[1][5];

				flg_out_node_p_num++;

				nbSize -= 6;
				pic_len += nbSize;

				memcpy(&bk_pic_buf[0][p_sn * 60000], &bk_pic_buf[1][6], nbSize);

				if (flg_out_node_p_num == p_num){
					//使用内存Stream方式获取图片
					HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE, pic_len);
					void *pData = GlobalLock(hGlobal);
					memcpy(pData, &bk_pic_buf[0], pic_len);
					GlobalUnlock(hGlobal);

					IStream *pStream = NULL;
					if (CreateStreamOnHGlobal(hGlobal, TRUE, &pStream) == S_OK)
					{
						if (SUCCEEDED(preview_image.Load(pStream))){
							//压缩成JPEG，并保存
							preview_image.Save("D:\\Primary\\WorkSpace\\All_Kinds_of_Programs\\C_Program\\ConsoleApplication1\\ConsoleApplication1\\1.jpg");
							preview_image.Destroy();
						}
						pStream->Release();
					}
					GlobalFree(hGlobal);

					//g_masterframe = cv::imread("./1.jpg", 1);
					//Mat masterframecopy;
					////resize(g_masterframe, masterframecopy, Size(g_masterwinwidth, g_masterwinheight));
					//g_fristdetectpoint = detectFaces(masterframecopy);
					//if (g_gettingcameraflag){
					//	imshow(MASTER_WIN_NAME, masterframecopy);

					//	waitKey(1);
					//	/*if (waitKey(1) == 27){
					//	break;
					//	}*/
					//}
				}
			}
			else if (nbSize == -1)
			{
				//get_pic = TRUE;
				break;
			}
		}

		sendto(listener, "SNAP:3", 6, 0, (sockaddr*)&sin_recv, sizeof(sin_recv));
		Sleep(80);
	}

	closesocket(listener);
	closesocket(sclient);
	WSACleanup();

	return 1;
}
#endif

void CFaceRecognitionAppApp::CaptureAndDisplay(int maxareasn, bool addfacesflag, bool concealedshootingflag)
{
	Mat tmpmat;
	//获得窗体在屏幕上的位置
	CRect masterwindowrect = GetMasterFramePoint();

	/*for (int i = 0; i < g_fristdetectpoint.size(); i++){
	cvtColor(g_masterframe(g_fristdetectpoint[i]), tmpmat, CV_BGR2GRAY);

	char ch_winname[50] = "";
	sprintf(ch_winname, "face_%d(%d)", i, snapnumber_);
	string winname = ch_winname;
	imshow(winname, tmpmat);
	moveWindow(winname, (masterwindowrect.left + masterwindowrect.Width() + 10 + g_fristdetectpoint[i].width * i), \
	(masterwindowrect.top + g_fristdetectpoint[i].width * snapnumber_));
	}*/

	// 只对框选面积最大的face进行录入
	if (g_fristdetectpoint.size() > 0 && snapnumber_ < CAPTURE_NUMBER){
		tmpmat = g_masterframe(g_fristdetectpoint[maxareasn]);
		if (g_fristdetectpoint[maxareasn].width > FACE_EXTRACT_MAX_SIDE_LEN ||
			g_fristdetectpoint[maxareasn].height > FACE_EXTRACT_MAX_SIDE_LEN){
			int maxlen = max(g_fristdetectpoint[maxareasn].width, g_fristdetectpoint[maxareasn].height);
			float ratio = (FACE_EXTRACT_MAX_SIDE_LEN * 1.0) / maxlen;
			resize(tmpmat, tmpmat, Size(g_fristdetectpoint[maxareasn].width * ratio, g_fristdetectpoint[maxareasn].height * ratio));
		}

		if (!concealedshootingflag){
			switch (snapnumber_)
			{
			case 0:
			{
				gedit_picface0->ShowWindow(TRUE);
				gedit_picface0->MoveWindow((masterwindowrect.left + masterwindowrect.Width() + 8), \
					(masterwindowrect.top + (FACE_EXTRACT_MAX_SIDE_LEN + 8) * snapnumber_), tmpmat.cols, tmpmat.rows, TRUE);

				imshow(CAPTURE_FACE0, tmpmat);
				break;
			}
			case 1:
			{
				gedit_picface1->ShowWindow(TRUE);
				gedit_picface1->MoveWindow((masterwindowrect.left + masterwindowrect.Width() + 8), \
					(masterwindowrect.top + (FACE_EXTRACT_MAX_SIDE_LEN + 8) * snapnumber_), tmpmat.cols, tmpmat.rows, TRUE);

				imshow(CAPTURE_FACE1, tmpmat);
				break;
			}
			case 2:
			{
				gedit_picface2->ShowWindow(TRUE);
				gedit_picface2->MoveWindow((masterwindowrect.left + masterwindowrect.Width() + 8), \
					(masterwindowrect.top + (FACE_EXTRACT_MAX_SIDE_LEN + 8) * snapnumber_), tmpmat.cols, tmpmat.rows, TRUE);

				imshow(CAPTURE_FACE2, tmpmat);
				break;
			}
			case 3:
			{
				gedit_picface3->ShowWindow(TRUE);
				gedit_picface3->MoveWindow((masterwindowrect.left + masterwindowrect.Width() + 8), \
					(masterwindowrect.top + (FACE_EXTRACT_MAX_SIDE_LEN + 8) * snapnumber_), tmpmat.cols, tmpmat.rows, TRUE);

				imshow(CAPTURE_FACE3, tmpmat);
				break;
			}
			case 4:
			{
				gedit_picface4->ShowWindow(TRUE);
				gedit_picface4->MoveWindow((masterwindowrect.left + masterwindowrect.Width() + 8), \
					(masterwindowrect.top + (FACE_EXTRACT_MAX_SIDE_LEN + 8) * snapnumber_), tmpmat.cols, tmpmat.rows, TRUE);

				imshow(CAPTURE_FACE4, tmpmat);
				break;
			}
			default:
				break;
			}
		}

		// 转为灰度图像之后入库
		cvtColor(tmpmat, tmpmat, CV_BGR2GRAY);
		g_facesimagemat.push_back(tmpmat);
		
		if (!addfacesflag)
			g_faceslabels.push_back(g_labels);
		else
			g_faceslabels.push_back(g_predicted[maxareasn]);

		if (!concealedshootingflag)
			snapnumber_++;
	}
}

void CFaceRecognitionAppApp::TrainAndStorage(int maxareasn, bool addfacesflag)
{
	// 判断g_labels号文件夹是否存在，若不存在则创建。
	CString labelspath;
	if (!addfacesflag){
		labelspath.Format("%s%d", g_faceslibpath, g_labels);
		if (!PathIsDirectory(labelspath)){
			if (!CreateDirectory(labelspath, NULL)){
				CString strMsg;
				strMsg.Format("创建路径\"%s\"失败！", labelspath);
				MessageBoxA(NULL, strMsg, "", MB_ICONWARNING);
				return;
			}
		}
	}
	else{
		labelspath.Format("%s%d", g_faceslibpath, g_predicted[maxareasn]);
		if (!PathIsDirectory(labelspath)){
			if (!CreateDirectory(labelspath, NULL)){
				CString strMsg;
				strMsg.Format("创建路径\"%s\"失败！", labelspath);
				MessageBoxA(NULL, strMsg, "", MB_ICONWARNING);
				return;
			}
		}
	}
	// 查询该labelspath文件夹中总文件数
	CFileFind filesfind;
	CString labelsfiles = labelspath + "\\*.* ";
	int filescounter = 0;
	BOOL res = filesfind.FindFile(labelsfiles);
	while (res){
		++filescounter;
		res = filesfind.FindNextFile();
	}
	filescounter -= 2;

	// 训练识别库
	string trainlibfile = g_faceslibpath + FACE_TRAIN_LIB;
	faces_model->train(g_facesimagemat, g_faceslabels);
	faces_model->save(trainlibfile);
	
	// 创建faceslabels.ini文件
	CStdioFile facesini;
	CString faceslabelswritestr;
	if (!facesini.Open(g_faceslibpath + FACE_LABELS_INI, CFile::modeWrite | CFile::typeText)){
		facesini.Open(g_faceslibpath + FACE_LABELS_INI, CFile::modeCreate | CFile::modeWrite | CFile::typeText);
	}
	else
		facesini.SeekToEnd();

	for (int i = g_facesimagemat.size() - CAPTURE_NUMBER; i < g_facesimagemat.size(); i++){
		char filename[256] = "";
		sprintf(filename, "%s\\%d.bmp", labelspath.GetBuffer(), filescounter++);
		imwrite(filename, g_facesimagemat[i]);

		if (!addfacesflag)
			faceslabelswritestr.Format("%s;%d;%s\n", filename, g_labels, g_peoplenames[g_labels].c_str());
		else
			faceslabelswritestr.Format("%s;%d;%s\n", filename, g_predicted[maxareasn], g_peoplenames[g_predicted[maxareasn]].c_str());
		facesini.WriteString(faceslabelswritestr);
	}
	facesini.Close();

	if (!addfacesflag)
		g_labels++;
	
	g_trainedflag = true;
}

int CFaceRecognitionAppApp::ExcitingMaxAreaFunction()
{
	int tmparea = 0, sn = 0;
	int area = g_fristdetectpoint[0].height * g_fristdetectpoint[0].width;

	for (int i = 1; i < g_fristdetectpoint.size(); i++){
		tmparea = g_fristdetectpoint[i].height * g_fristdetectpoint[i].width;
		if (tmparea > area){
			area = tmparea;
			sn = i;
		}
	}

	return sn;
}

CRect CFaceRecognitionAppApp::GetMasterFramePoint()
{
	// 获得主显示窗口的绝对坐标
	CRect masterframerect;
	::GetWindowRect(ghwnd_idmasterframe, &masterframerect); //ID_MASTER_FRAME为Picture Control的ID

	return masterframerect;
}

int CFaceRecognitionAppApp::Mat2CImage(Mat *mat, CImage &img) {
	if (!mat || mat->empty())
		return -1;
	int nBPP = mat->channels() * 8;
	img.Create(mat->cols, mat->rows, nBPP);
	if (nBPP == 8)
	{
		static RGBQUAD pRGB[256];
		for (int i = 0; i < 256; i++)
			pRGB[i].rgbBlue = pRGB[i].rgbGreen = pRGB[i].rgbRed = i;
		img.SetColorTable(0, 256, pRGB);
	}
	uchar* psrc = mat->data;
	uchar* pdst = (uchar*)img.GetBits();
	int imgPitch = img.GetPitch();
	for (int y = 0; y < mat->rows; y++)
	{
		memcpy(pdst, psrc, mat->cols*mat->channels());//mat->step is incorrect for those images created by roi (sub-images!)  
		psrc += mat->step;
		pdst += imgPitch;
	}

	return 0;
}
