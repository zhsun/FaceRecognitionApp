// BottomLayer.cpp : implementation file
//

#include "BottomLayer.h"

// BottomLayer dialog
HWND ghwnd_bottomlayer;

IMPLEMENT_DYNAMIC(BottomLayer, CDialogEx)

BottomLayer::BottomLayer(CWnd* pParent /*=NULL*/)
	: CDialogEx(BottomLayer::IDD, pParent)
{

}

BottomLayer::~BottomLayer()
{
}

void BottomLayer::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	// 获取控件指针
	cwndbottomlayer_ = GetDlgItem(ID_BOTTOMLAYER_DIALOG);

	// 改变该对话框的透明度
	SetWindowLong(GetSafeHwnd(), GWL_EXSTYLE, GetWindowLong(GetSafeHwnd(), GWL_EXSTYLE) | WS_EX_LAYERED);
	SetLayeredWindowAttributes(GRAY_COLOR, 0, LWA_ALPHA); //0为完全透明，255为完全不透明
}


BEGIN_MESSAGE_MAP(BottomLayer, CDialogEx)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// BottomLayer message handlers


void BottomLayer::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here
	CRect rc;
	GetClientRect(&rc); // 获取客户区域大小
	dc.FillSolidRect(rc, GRAY_COLOR);
}

