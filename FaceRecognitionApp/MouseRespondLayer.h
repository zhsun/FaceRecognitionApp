#pragma once
#include "stdafx.h"
#include "FaceRecognitionApp.h"
#include "BottomLayer.h"
#include "afxdialogex.h"
#include "config.h"
#include "afxwin.h"

// CMouseRespondLayer dialog

class CMouseRespondLayer : public CDialogEx
{
	DECLARE_DYNAMIC(CMouseRespondLayer)

public:
	CMouseRespondLayer(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMouseRespondLayer();

// Dialog Data
	enum { IDD = ID_MOUSERESPONDLAYER_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void InsertIcon(bool addfacesimgflag = false);
	inline void ClearArea();

private:
	static bool dragflag_;
	static bool menupopupflag_;
	static bool menuclickflag_;
	static bool triggerflag_;
	static bool addfacesimgflag_;
	static int  maxareasn_;
	int  offsetx_;
	int	 offsety_;
	int  animationeffectsstep_;
	CFont  fontH20_;
	CFont  fontH40_;
	CPoint originpoint_;
	CPoint lastpoint_;
	CImage iconlib_[10];
	CString addfacename_;
	CEdit *nameedit_;
	BottomLayer bottomlayer_;

public:
	CEdit personname;
};

extern HWND ghwnd_mouserespondlayer;
extern CWnd* gedit_picface0;
extern CWnd* gedit_picface1;
extern CWnd* gedit_picface2;
extern CWnd* gedit_picface3;
extern CWnd* gedit_picface4;