#pragma once
#include "stdafx.h"
#include "FaceRecognitionApp.h"
#include "afxdialogex.h"
#include "config.h"

// BottomLayer dialog

class BottomLayer : public CDialogEx
{
	DECLARE_DYNAMIC(BottomLayer)

public:
	BottomLayer(CWnd* pParent = NULL);   // standard constructor
	virtual ~BottomLayer();

public:
	CWnd* cwndbottomlayer_;

// Dialog Data
	enum { IDD = ID_BOTTOMLAYER_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};

extern HWND ghwnd_bottomlayer;
