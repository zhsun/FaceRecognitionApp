#ifndef _MICROSOFTTTSVOICE_H_
#define _MICROSOFTTTSVOICE_H_
#include <iostream>
// 微软TTS语音库
#include "sapi.h"
#include "sphelper.h"

class CTtsSpeaker
{
public:
	CTtsSpeaker();   // 标准构造函数
	~CTtsSpeaker();

	bool SetVolume(int volume);
	bool Speaker(LPCWSTR pwcs);

private:
	CLSID CLSID_SpVoice_;
	ISpVoice *pSpVoice_;
	IEnumSpObjectTokens *pSpEnumTokens_;
	ULONG ulTokensNumber_;
	ISpObjectToken *pSpToken_;
};
#endif //_MICROSOFTTTSVOICE_H_