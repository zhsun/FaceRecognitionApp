#include "MicrosoftTtsVoice.h"


CTtsSpeaker::CTtsSpeaker()
{
	pSpVoice_ = NULL;
	pSpEnumTokens_ = NULL;
	pSpToken_ = NULL;
	ulTokensNumber_ = 0;

	::CoInitialize(NULL);             // COM初始化
	CLSIDFromProgID(OLESTR("SAPI.SpVoice"), &CLSID_SpVoice_);
	CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_INPROC_SERVER, IID_ISpVoice, (void**)&pSpVoice_);
	SpEnumTokens(SPCAT_VOICES, NULL, NULL, &pSpEnumTokens_);
	pSpEnumTokens_->GetCount(&ulTokensNumber_);
	pSpEnumTokens_->Item(2, &pSpToken_);//上面代码可以知道语音包的序号多少
	pSpVoice_->SetVoice(pSpToken_);
}

CTtsSpeaker::~CTtsSpeaker()
{
	pSpVoice_->Release();
	pSpEnumTokens_->Release();
	::CoUninitialize();
}

bool CTtsSpeaker::SetVolume(int volume)
{
	pSpVoice_->SetVolume(volume);

	return true;
}

bool CTtsSpeaker::Speaker(LPCWSTR pwcs)
{
	pSpVoice_->Speak(pwcs, SPF_ASYNC, NULL); //可朗读中文和英文的混合字符串 SPF_ASYNC:异步朗读，不会阻塞 SPF_DEFAULT:阻塞

	return true;
}

/***********范例代码***********/
/************************************************************
void speaker()
{
	::CoInitialize(NULL);             // COM初始化
	CLSID CLSID_SpVoice;
	CLSIDFromProgID(OLESTR("SAPI.SpVoice"), &CLSID_SpVoice);
	ISpVoice *pSpVoice = NULL;
	IEnumSpObjectTokens *pSpEnumTokens = NULL;
	CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_INPROC_SERVER, IID_ISpVoice, (void**)&pSpVoice);
	SpEnumTokens(SPCAT_VOICES, NULL, NULL, &pSpEnumTokens);
	ULONG ulTokensNumber = 0;
	pSpEnumTokens->GetCount(&ulTokensNumber);
	ISpObjectToken *pSpToken = NULL;
	pSpEnumTokens->Item(2, &pSpToken);//上面代码可以知道语音包的序号多少
	pSpVoice->SetVoice(pSpToken);
	pSpVoice->SetVolume(80);
	pSpVoice->Speak(L"叮咚，欢迎光临", SPF_DEFAULT, NULL);     // 朗读中文和英文的混合字符串 SPF_ASYNC:异步朗读，不会阻塞
	pSpVoice->Release();
	pSpEnumTokens->Release();
	::CoUninitialize();
	return (void)0;
}
************************************************************/