#ifndef _CONFIG_H_
#define _CONFIG_H_

// 让MFC支持printf调试
#include <io.h>    
#include <fcntl.h>
#include <cstring>
#include <afx.h>

#include "CvZhCnText.h"
#include "MicrosoftTtsVoice.h"

using namespace cv;
using namespace std;

#define MASTER_WIN_WIDTH  640
#define	MASTER_WIN_HEIGHT 480
#define MASTER_WIN_NAME   "master_frame"
#define CAPTURE_FACE0	  "capture_face0"
#define CAPTURE_FACE1	  "capture_face1"
#define CAPTURE_FACE2	  "capture_face2"
#define CAPTURE_FACE3	  "capture_face3"
#define CAPTURE_FACE4	  "capture_face4"


#define DETECT_FACES_MAX  20

#define CAPTURE_NUMBER	5
#define FACE_EXTRACT_MAX_SIDE_LEN	90	
#define FACE_TRAIN_LIB "trainfaces.bin"
#define FACE_LABELS_INI "faceslabels.ini"

//#define FACE_TRACK

#define BASE_OFFSET 5

//画笔的颜色定义
#define EMPTY_COLOR			(COLORREF)-1
#define BLACK_COLOR			RGB(0,0,0)
#define WHITE_COLOR			RGB(255,255,255)
#define RED_COLOR			RGB(255,0,0)
#define GREEN_COLOR			RGB(0,255,0)
#define BLUE_COLOR			RGB(0,0,255)
#define YELLOW_COLOR		RGB(255,255,0)
#define PINK_COLOR			RGB(255,192,192)
#define GRAY_COLOR			RGB(150,150,150)
#define ORANG_COLOR			RGB(255,153,18)
#define ORANG_BOOTOM_COLOR	RGB(255,150, 0)

typedef struct _rgb_
{
	unsigned int r;
	unsigned int g;
	unsigned int b;
}RGB;

typedef struct _point_sn_
{
	int newpointsn[DETECT_FACES_MAX ^ 2];
	int lastpointsn[DETECT_FACES_MAX ^ 2];
}POINT_SN;

typedef struct _point_sn_ex_
{
	int newpointsn[DETECT_FACES_MAX];
	int lastpointsn[DETECT_FACES_MAX];
}POINT_SN_EX;

// 全局函数
extern void InitConsoleWindow();// 让MFC支持printf调试

// 全局变量
extern int g_labels;
extern int g_masterwinwidth;
extern int g_masterwinheight;
extern int g_predicted[DETECT_FACES_MAX];
extern double g_threshold;
extern CString g_currentpath;
extern CString g_faceslibpath;
extern bool g_getcameraoverflag;
extern bool g_gettingcameraflag;
extern bool g_trainedflag;
extern bool g_speakerflag;
extern string g_peoplenames[1000];
extern Mat g_masterframe;
extern std::vector<Rect> g_fristdetectpoint;
extern cv::vector<Mat> g_facesimagemat;
extern cv::vector<int> g_faceslabels;
extern CTtsSpeaker g_speaker;

// 全局RGB颜色
extern byte g_rectrgb[300];


// 临时全局变量，调试用
//extern FILE* newpointfile;
//extern FILE* lastpointfile;
//extern FILE* prelog;

#endif