#include <opencv.hpp>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

static void read_csv(const string& filename, cv::vector<Mat>& images, cv::vector<int>& labels, char separator = ';') {
	std::ifstream file(filename.c_str(), ifstream::in);
	if (!file) {
		string error_message = "No valid input file was given, please check the given filename.";
		CV_Error(CV_StsBadArg, error_message);
	}
	string line, img_path, classlabel;
	while (getline(file, line)) {
		stringstream liness(line);
		getline(liness, img_path, separator);
		getline(liness, classlabel);
		if (!img_path.empty() && !classlabel.empty()) {
			images.push_back(imread(img_path, 0));
			labels.push_back(atoi(classlabel.c_str()));
		}
	}
}