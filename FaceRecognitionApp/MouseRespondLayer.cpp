// MouseRespondLayer.cpp : implementation file
//

#include "MouseRespondLayer.h"

HWND ghwnd_mouserespondlayer;
CWnd* gedit_picface0;
CWnd* gedit_picface1;
CWnd* gedit_picface2;
CWnd* gedit_picface3;
CWnd* gedit_picface4;

int CMouseRespondLayer::maxareasn_ = -1;

bool CMouseRespondLayer::dragflag_ = false;
bool CMouseRespondLayer::menupopupflag_ = false;
bool CMouseRespondLayer::menuclickflag_ = false;
bool CMouseRespondLayer::triggerflag_ = false;
bool CMouseRespondLayer::addfacesimgflag_ = false;

// CMouseRespondLayer dialog

IMPLEMENT_DYNAMIC(CMouseRespondLayer, CDialogEx)

CMouseRespondLayer::CMouseRespondLayer(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMouseRespondLayer::IDD, pParent)
{
	fontH20_.CreateFont(
		20,						//   nHeight
		0,                      //   nWidth
		0,                      //   nEscapement
		0,                      //   nOrientation
		FW_NORMAL,              //   nWeight
		FALSE,                  //   bItalic
		FALSE,                  //   bUnderline
		0,                      //   cStrikeOut
		ANSI_CHARSET,           //   nCharSet
		OUT_DEFAULT_PRECIS,     //   nOutPrecision
		CLIP_DEFAULT_PRECIS,    //   nClipPrecision
		DEFAULT_QUALITY,                       //   nQuality
		DEFAULT_PITCH | FF_SWISS,     //   nPitchAndFamily
		_T("幼圆")
		);

	fontH40_.CreateFont(
		40,						//   nHeight
		0,                      //   nWidth
		0,                      //   nEscapement
		0,                      //   nOrientation
		FW_NORMAL,              //   nWeight
		FALSE,                  //   bItalic
		FALSE,                  //   bUnderline
		0,                      //   cStrikeOut
		ANSI_CHARSET,           //   nCharSet
		OUT_DEFAULT_PRECIS,     //   nOutPrecision
		CLIP_DEFAULT_PRECIS,    //   nClipPrecision
		DEFAULT_QUALITY,                       //   nQuality
		DEFAULT_PITCH | FF_SWISS,     //   nPitchAndFamily
		_T("幼圆")
		);
}

CMouseRespondLayer::~CMouseRespondLayer()
{
}

void CMouseRespondLayer::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	// 获取三个图片控件的指针，以便在另一个Dialog中调用
	gedit_picface0 = GetDlgItem(ID_PIC_FACE0);
	gedit_picface1 = GetDlgItem(ID_PIC_FACE1);
	gedit_picface2 = GetDlgItem(ID_PIC_FACE2);
	gedit_picface3 = GetDlgItem(ID_PIC_FACE3);
	gedit_picface4 = GetDlgItem(ID_PIC_FACE4);

	// 改变该对话框的透明度
	SetWindowLong(GetSafeHwnd(), GWL_EXSTYLE, GetWindowLong(GetSafeHwnd(), GWL_EXSTYLE) | WS_EX_LAYERED);
	SetLayeredWindowAttributes(ORANG_BOOTOM_COLOR, 0, LWA_COLORKEY); //0为完全透明，255为完全不透明

	// 把被捕捉到的face的显示窗口关联上图片控件上
	{
		HWND hWnd;
		HWND hParent;
		namedWindow(CAPTURE_FACE0, WINDOW_AUTOSIZE);
		hWnd = (HWND)cvGetWindowHandle(CAPTURE_FACE0);
		hParent = ::GetParent(hWnd);
		::SetParent(hWnd, GetDlgItem(ID_PIC_FACE0)->m_hWnd);
		::ShowWindow(hParent, SW_HIDE);

		namedWindow(CAPTURE_FACE1, WINDOW_AUTOSIZE);
		hWnd = (HWND)cvGetWindowHandle(CAPTURE_FACE1);
		hParent = ::GetParent(hWnd);
		::SetParent(hWnd, GetDlgItem(ID_PIC_FACE1)->m_hWnd);
		::ShowWindow(hParent, SW_HIDE);

		namedWindow(CAPTURE_FACE2, WINDOW_AUTOSIZE);
		hWnd = (HWND)cvGetWindowHandle(CAPTURE_FACE2);
		hParent = ::GetParent(hWnd);
		::SetParent(hWnd, GetDlgItem(ID_PIC_FACE2)->m_hWnd);
		::ShowWindow(hParent, SW_HIDE);

		namedWindow(CAPTURE_FACE3, WINDOW_AUTOSIZE);
		hWnd = (HWND)cvGetWindowHandle(CAPTURE_FACE3);
		hParent = ::GetParent(hWnd);
		::SetParent(hWnd, GetDlgItem(ID_PIC_FACE3)->m_hWnd);
		::ShowWindow(hParent, SW_HIDE);

		namedWindow(CAPTURE_FACE4, WINDOW_AUTOSIZE);
		hWnd = (HWND)cvGetWindowHandle(CAPTURE_FACE4);
		hParent = ::GetParent(hWnd);
		::SetParent(hWnd, GetDlgItem(ID_PIC_FACE4)->m_hWnd);
		::ShowWindow(hParent, SW_HIDE);
	}

	// 隐藏输入文字的控件
	nameedit_ = (CEdit*)GetDlgItem(ID_NAME_EDIT);
	nameedit_->ShowWindow(FALSE); //隐藏该控件
	DDX_Control(pDX, ID_NAME_EDIT, personname);
}


BEGIN_MESSAGE_MAP(CMouseRespondLayer, CDialogEx)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_PAINT()
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CMouseRespondLayer message handlers

void CMouseRespondLayer::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CRect rc;
	GetClientRect(&rc); // 获取客户区域大小
	dc.FillSolidRect(rc, ORANG_BOOTOM_COLOR);
}

#define ICO_INSERT_METHOD	1

// 在指定区域内响应鼠标左键按下操作
void CMouseRespondLayer::OnLButtonDown(UINT nFlags, CPoint point)
{
	// 使得该鼠标层毫无响应，调试用。
	//menupopupflag_ = true;
	
	// TODO: Add your message handler code here and/or call default
	CRect masterframerect = theApp.GetMasterFramePoint();

	if (point.x >= masterframerect.left &&
		point.x <= masterframerect.left + g_masterwinwidth - 1 &&
		point.y >= masterframerect.top &&
		point.y <= masterframerect.top + g_masterwinheight - 1){
		
		triggerflag_ = true;

		if (!menupopupflag_){
			dragflag_ = true;
			originpoint_ = point;
			lastpoint_ = originpoint_;

			theApp.snapnumber_ = 0;
			if (g_fristdetectpoint.size() > 0)
				maxareasn_ = theApp.ExcitingMaxAreaFunction();
			
			menuclickflag_ = true;
		}
#if 1
		else // 按下 按钮1“抓拍”
			if (point.x >= originpoint_.x + offsetx_ &&
				point.x <= originpoint_.x + offsetx_ + iconlib_[0].GetWidth() &&
				point.y >= originpoint_.y &&
				point.y <= originpoint_.y + iconlib_[0].GetHeight()){

				if (!addfacesimgflag_)
					theApp.CaptureAndDisplay(maxareasn_);
				else
					theApp.CaptureAndDisplay(maxareasn_, addfacesimgflag_);

				if (theApp.snapnumber_ >= CAPTURE_NUMBER){ // 已抓拍完指定数量的照片后锁定抓拍功能，弹出姓名输入框
					CFont editfont;
					editfont.CreatePointFont(150, "宋体");

					GetDlgItem(ID_TEXT_NAME)->SetFont(&editfont);
					GetDlgItem(ID_TEXT_NAME)->MoveWindow(originpoint_.x + offsetx_ + iconlib_[0].GetWidth() + BASE_OFFSET, \
						originpoint_.y + offsety_ + ((iconlib_[1].GetHeight() / 2) - (30 / 2)), \
						50, 30, TRUE);
					GetDlgItem(ID_TEXT_NAME)->ShowWindow(TRUE);

					nameedit_->SetFont(&editfont, FALSE);
					nameedit_->MoveWindow(originpoint_.x + offsetx_ + iconlib_[0].GetWidth() + BASE_OFFSET + 50, \
						originpoint_.y + offsety_ + ((iconlib_[1].GetHeight() / 2) - (30 / 2)), \
						FACE_EXTRACT_MAX_SIDE_LEN, 30, TRUE);		
					nameedit_->ShowWindow(TRUE); // 显示该控件

					if (addfacesimgflag_){
						personname.SetReadOnly(true);
						personname.SetWindowTextA(addfacename_);
					}
				}

				//theApp.CaptureAndDisplay(maxareasn_, true);
			}
			else // 按下 按钮2“注册”
				if (point.x >= originpoint_.x + offsetx_ &&
					point.x <= originpoint_.x + offsetx_ + iconlib_[1].GetWidth() &&
					point.y >= originpoint_.y + offsety_ &&
					point.y <= originpoint_.y + offsety_ + iconlib_[1].GetHeight()){

					if (theApp.snapnumber_ >= CAPTURE_NUMBER){ // 已抓拍完指定数量的照片后锁定抓拍功能，并使能注册功能
						// 隐藏采集的face图像窗口
						GetDlgItem(ID_PIC_FACE0)->ShowWindow(FALSE);
						GetDlgItem(ID_PIC_FACE1)->ShowWindow(FALSE);
						GetDlgItem(ID_PIC_FACE2)->ShowWindow(FALSE);

						if (!addfacesimgflag_){
							personname.GetWindowTextA(theApp.personname_);
							g_peoplenames[g_labels] = CT2A(theApp.personname_);
							GetDlgItem(ID_TEXT_NAME)->ShowWindow(FALSE);
							nameedit_->ShowWindow(FALSE);
							personname.SetWindowTextA("");
								
							theApp.TrainAndStorage();
						}
						else{
							personname.SetReadOnly(false);
							GetDlgItem(ID_TEXT_NAME)->ShowWindow(FALSE);
							nameedit_->ShowWindow(FALSE);
							personname.SetWindowTextA("");

							theApp.TrainAndStorage(maxareasn_, addfacesimgflag_);
						}

						// 消除上一次出现文字或图标的区域
						ClearArea();
						menupopupflag_ = false;
						menuclickflag_ = false;
						triggerflag_ = false;
						dragflag_ = false;
					}
				}
				else // 按下 按钮3“设置”
					if (point.x >= originpoint_.x + offsetx_ &&
						point.x <= originpoint_.x + offsetx_ + iconlib_[2].GetWidth() &&
						point.y >= originpoint_.y + offsety_ * 2 &&
						point.y <= originpoint_.y + offsety_ * 2 + iconlib_[2].GetHeight()){
						
						
					}
					else{
						menuclickflag_ = false;
					}
#endif
	}
	else
		triggerflag_ = false;
	
	CDialogEx::OnLButtonDown(nFlags, point);
}

// 在指定区域内响应鼠标左键移动操作
void CMouseRespondLayer::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (!menupopupflag_ && dragflag_){
		// 定义画笔
		CPen pen(PS_SOLID, 10, ORANG_COLOR);

		// 画出鼠标滑动的轨迹
		CClientDC dc(this);
		dc.SelectObject(pen);
		dc.MoveTo(lastpoint_);
		dc.LineTo(point);
		lastpoint_ = point;

		pen.DeleteObject();
	}

	CDialogEx::OnMouseMove(nFlags, point);
}

// 在指定区域内响应鼠标左键弹起操作
void CMouseRespondLayer::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if (!menupopupflag_ && triggerflag_){
		dragflag_ = false;

		// 消除上一次画笔画过的区域
		ClearArea();

		::SetLayeredWindowAttributes(ghwnd_bottomlayer, GRAY_COLOR, 200, LWA_ALPHA); //0为完全透明，255为完全不透明
 
		// 插入菜单图标
		if (g_predicted[maxareasn_] >= 0){
			addfacesimgflag_ = true;
			addfacename_ = g_peoplenames[g_predicted[maxareasn_]].c_str();
			InsertIcon(addfacesimgflag_);
		}	
		else
			InsertIcon();

		menupopupflag_ = true;
	}
	else if (!menuclickflag_){
		::SetLayeredWindowAttributes(ghwnd_bottomlayer, GRAY_COLOR, 0, LWA_ALPHA); //0为完全透明，255为完全不透明
		
		// 消除上一次出现文字或图标的区域
		ClearArea();

		menupopupflag_ = false;
		addfacesimgflag_ = false;
	}
	
	CDialogEx::OnLButtonUp(nFlags, point);
}

// 插入菜单图标
void CMouseRespondLayer::InsertIcon(bool addfacesimgflag)
{
	iconlib_[0].Destroy();
	iconlib_[1].Destroy();
	iconlib_[2].Destroy();

	string iconpath = g_currentpath + "res\\";
	// 在指定位置插入图片
	if (!addfacesimgflag){
		string iconfile0 = iconpath + "photo_camera.png";
		iconlib_[0].Load(iconfile0.c_str());
		if (iconlib_[0].IsNull()){
			MessageBox(_T("加载 photo_camera.png 失败"));
			return;
		}
	}
	else{
		string iconfile0 = iconpath + "photo_camera_plus.png";
		iconlib_[0].Load(iconfile0.c_str());
		if (iconlib_[0].IsNull()){
			MessageBox(_T("加载 photo_camera_plus.png 失败"));
			return;
		}
	}
	string iconfile1 = iconpath + "register.png";
	iconlib_[1].Load(iconfile1.c_str());
	if (iconlib_[0].IsNull()){
		MessageBox(_T("加载 register.png 失败"));
		return;
	}
	string iconfile2 = iconpath + "settings.png";
	iconlib_[2].Load(iconfile2.c_str());
	if (iconlib_[0].IsNull()){
		MessageBox(_T("加载 settings.png 失败"));
		return;
	}
	
	offsetx_ = BASE_OFFSET;
	offsety_ = iconlib_[0].GetHeight() + BASE_OFFSET;

#if ICO_INSERT_METHOD == 1
	// 插入图标 方法1：
	// 为将png的背景图层变为透明，做一次转换(好像并不需要这一步)
	//byte *pByte;
	//if (iconlib_[0].GetBPP() == 32){ //确认该图像包含Alpha通道
	//	for (int i = 0; i < iconlib_[0].GetWidth(); i++){
	//		for (int j = 0; j < iconlib_[0].GetHeight(); j++){
	//			pByte = (byte *)iconlib_[0].GetPixelAddress(i, j);
	//			pByte[0] = (pByte[0] * pByte[3] / 255) > 0.5 ? 1 : 0;
	//			pByte[1] = (pByte[1] * pByte[3] / 255) > 0.5 ? 1 : 0;
	//			pByte[2] = (pByte[2] * pByte[3] / 255) > 0.5 ? 1 : 0;

	//			pByte = (byte *)iconlib_[1].GetPixelAddress(i, j);
	//			pByte[0] = (pByte[0] * pByte[3] / 255) > 0.5 ? 1 : 0;
	//			pByte[1] = (pByte[1] * pByte[3] / 255) > 0.5 ? 1 : 0;
	//			pByte[2] = (pByte[2] * pByte[3] / 255) > 0.5 ? 1 : 0;

	//			pByte = (byte *)iconlib_[2].GetPixelAddress(i, j);
	//			pByte[0] = (pByte[0] * pByte[3] / 255) > 0.5 ? 1 : 0;
	//			pByte[1] = (pByte[1] * pByte[3] / 255) > 0.5 ? 1 : 0;
	//			pByte[2] = (pByte[2] * pByte[3] / 255) > 0.5 ? 1 : 0;
	//		}
	//	}
	//}

	//iconlib_[0].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y);
	//iconlib_[1].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y + offsety_);
	//iconlib_[2].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y + offsety_ * 2z);
	animationeffectsstep_ = 1;
	SetTimer(ID_MENU_ICON_ANIMATION_EFFECTS, 20, NULL);

#elif ICO_INSERT_METHOD == 2
	// 插入图标 方法2：
	dc.SetStretchBltMode(COLORONCOLOR);
	StretchBlt(dc.GetSafeHdc(), originpoint_.x + 5, originpoint_.y, 100, 100, iconlib_[0].GetDC(), 0, 0, iconlib_[0].GetWidth(), iconlib_[0].GetHeight(), SRCCOPY); //缩放贴图

#elif ICO_INSERT_METHOD == 3	
	// 插入图标 方法3：
	CDC cdc;
	cdc.CreateCompatibleDC(NULL);
	dc.BitBlt(originpoint_.x + 5, originpoint_.y, iconlib_[0].GetWidth(), iconlib_[0].GetHeight(), &cdc, 0, 0, SRCCOPY);

#elif ICO_INSERT_METHOD == 4
	// 在指定位置插入文字
	dc.SelectObject(fontH40_);
	dc.SetBkColor(ORANG_BOOTOM_COLOR);
	dc.SetTextColor(ORANG_COLOR);
	dc.TextOutA(originpoint_.x + 5, originpoint_.y, "抓拍");
	Sleep(50);
	dc.TextOutA(originpoint_.x + 5, originpoint_.y + 40, "连续抓拍");
	Sleep(50);
	dc.TextOutA(originpoint_.x + 5, originpoint_.y + 40 * 2, "注册");
#endif
}


void CMouseRespondLayer::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	CDC *pDC = GetDC();

	if (nIDEvent == ID_MENU_ICON_ANIMATION_EFFECTS){
		switch (animationeffectsstep_)
		{
		case 1: {
			//iconlib_[0].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y);
			iconlib_[2].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y - 10);
			animationeffectsstep_++;
			break;
		}
		case 2: {
			ClearArea(); // 消除上一次出现文字或图标的区域
			iconlib_[1].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y - 10);
			iconlib_[2].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y + 10);
			animationeffectsstep_++;
			break;
		}
		case 3: {
			ClearArea(); // 消除上一次出现文字或图标的区域
			iconlib_[0].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y - 10);
			iconlib_[1].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y - 5);
			iconlib_[2].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y + 35);
			animationeffectsstep_++;
			break;
		}
		case 4: {
			ClearArea(); // 消除上一次出现文字或图标的区域
			iconlib_[0].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y - 5);
			iconlib_[1].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y);
			iconlib_[2].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y + offsety_);
			animationeffectsstep_++;
			break;
		}
		case 5: {
			ClearArea(); // 消除上一次出现文字或图标的区域
			iconlib_[0].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y);
			iconlib_[1].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y + 35);
			iconlib_[2].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y + offsety_ + 35);
			animationeffectsstep_++;
			break;
		}
		case 6: {
			ClearArea(); // 消除上一次出现文字或图标的区域
			iconlib_[0].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y);
			iconlib_[1].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y + offsety_);
			iconlib_[2].Draw(pDC->m_hDC, originpoint_.x + offsetx_, originpoint_.y + offsety_ * 2);
			animationeffectsstep_++;
			break;
		}
		case 7: {
			KillTimer(ID_MENU_ICON_ANIMATION_EFFECTS);
			break;
		}
		default:
			break;
		}
	}

	CDialogEx::OnTimer(nIDEvent);
}

inline void CMouseRespondLayer::ClearArea()
{
	CRect rc;
	CClientDC dc(this);
	CBrush brush;
	brush.CreateSolidBrush(COLORREF(ORANG_BOOTOM_COLOR));
	GetClientRect(&rc); // 获取客户区域大小
	dc.FillRect(rc, &brush);
}
