#include <algorithm>
#include "FaceDetect.h"
#include "FaceRotate.h"
#include "ReadCSV.h"

bool fristruningflag = true;
Ptr<FaceRecognizer> faces_model;
std::vector<Rect> lastdetectpoints;

void Dlib_Predefine()
{
	string datpath = g_currentpath + "config\\" + "shape_predictor_68_face_landmarks.dat";
	deserialize(datpath) >> sp;//读入标记点文件

	//string prelogpath = g_currentpath + "pre.log";
	//prelog = fopen(prelogpath.c_str(), "w+");
}

Mat FaceToOne(Mat source)//归一化处理函数
{
	string maskpath = g_currentpath + "res\\" + "mask2.jpg";
	equalizeHist(source, source);//直方图均衡
	Mat Mask = imread(maskpath, 0);
	resize(source, source, Size(FACE_EXTRACT_MAX_SIDE_LEN, FACE_EXTRACT_MAX_SIDE_LEN));//裁剪
	//resize(source, source, Size(Mask.cols, Mask.rows));//裁剪
	Mat changedMask;
	source.copyTo(changedMask, Mask);

	return changedMask;
}

//人脸对齐技术 提高准确率
Mat FaceAlignment(Mat frame, Rect point)
{
	std::vector<full_object_detection> shapes;

	dlib::rectangle dlibRect((long)point.tl().x, (long)point.tl().y, (long)point.br().x - 1, (long)point.br().y - 1);
	dlib::full_object_detection shape = sp(dlib::cv_image<uchar>(frame), dlibRect);//标记点
	shapes.push_back(shape);//把点保存在了shape中

	dlib::array<array2d<rgb_pixel>>  face_chips;
	extract_image_chips(dlib::cv_image<uchar>(frame), get_face_chip_details(shapes), face_chips);
	Mat pic = toMat(face_chips[0]);
	cvtColor(pic, pic, CV_BGR2GRAY);

	return FaceToOne(pic);
}

std::vector<Rect> detectFaces(Mat frame)
{
	Mat gray;
	std::vector<Rect> points;
	Rect point;
	cvtColor(frame, gray, CV_BGR2GRAY);
	int *pResults = NULL;
	int facesnumber = 0;
	int neighbors;

	unsigned char * pBuffer = (unsigned char *)malloc(DETECT_BUFFER_SIZE);
	if (!pBuffer){
		fprintf(stderr, "Can not alloc buffer.\n");
		return points;
	}

	// frontal face detection 
	// it's fast, but cannot detect side view faces
	//pResults = facedetect_frontal(pBuffer, (unsigned char*)(gray.ptr(0)), gray.cols, gray.rows, (int)gray.step,\
					1.2f, 2, 48);

	// multiview face detection 
	// it can detect side view faces, but slower than facedetect_frontal().
	//pResults = facedetect_multiview(pBuffer, (unsigned char*)(gray.ptr(0)), gray.cols, gray.rows, gray.step,\
			1.2f, 5, 48);

	// reinforced multiview face detection 
	// it can detect side view faces, better but slower than facedetect_multiview().
	pResults = facedetect_multiview_reinforce(pBuffer, (unsigned char*)(gray.ptr(0)), gray.cols, gray.rows, gray.step, \
		1.2f, 5, 48);
	//printf("%d faces detected.\n", (pResults ? *pResults : 0));//重复运行

	// 设置框选人脸上限数量
	facesnumber = (pResults ? *pResults : 0);
	facesnumber = facesnumber > DETECT_FACES_MAX ? DETECT_FACES_MAX : facesnumber;

	for (int i = 0; i < facesnumber; i++){
		short * p = ((short*)(pResults + 1)) + 6 * i;
		point.x = p[0];
		point.y = p[1];
		point.width = p[2];
		point.height = p[3];
		neighbors = p[4];
		points.push_back(point);
	}

	free(pBuffer);

	// 触发语音程序
	if (!g_speakerflag){
		if (facesnumber == 1){
			g_speaker.Speaker(L"叮咚，欢迎您光临");
			g_speakerflag = true;
		}
		else if (facesnumber > 1){
			g_speaker.Speaker(L"叮咚，欢迎大家光临");
			g_speakerflag = true;
		}
	}

#ifdef FACE_TRACK
	std::vector<Rect> resultpoints;

	if (facesnumber == 0)
		fristruningflag = true;
	else
		resultpoints = trackFaces(points);

	for (int i = 0; i < resultpoints.size(); i++){
		boxFacesAndMark(frame, resultpoints[i], i);
	}

	return resultpoints;
#else
	if (g_trainedflag){
		Mat tmpmat;
		for (int i = 0; i < points.size(); i++){
			/*tmpmat = gray(points[i]);
			if (points[i].width > FACE_EXTRACT_MAX_SIDE_LEN ||
			points[i].height > FACE_EXTRACT_MAX_SIDE_LEN){
			int maxlen = max(points[i].width, points[i].height);
			float ratio = (FACE_EXTRACT_MAX_SIDE_LEN * 1.0) / maxlen;
			resize(tmpmat, tmpmat, Size(points[i].width * ratio, points[i].height * ratio));
			}*/
			tmpmat = FaceAlignment(gray, points[i]);
			//predicted = faces_model->predict(tmpmat);
			double confidence = 0.0;
			faces_model->predict(tmpmat, g_predicted[i], confidence);

			//if (confidence > 2650)
			//	fprintf(prelog, "person: %d\tpre:%d\tname:%s\tthreshold:%1.6f\n", i, g_predicted[i], g_peoplenames[g_predicted[i]].c_str(), confidence);

			boxFacesAndMark(frame, points[i], i, g_predicted[i]);
		}
	}
	else{
		for (int i = 0; i < points.size(); i++){
			boxFacesAndMark(frame, points[i], i);
		}
	}

	return points;
#endif
}

inline void boxFacesAndMark(Mat frame, Rect point, int sn, int predicted)
{
	string fontpath = g_currentpath + "Font\\" + "msyh.ttc";
	CvxText text(fontpath.c_str());

	if (predicted < 0){
		cv::rectangle(frame, Rect(point.x - 2, point.y - 26, 110, 26), Scalar(230, 255, 0), -1);
		
		char msg[50] = "";
		sprintf(msg, "围观群众%c", 'A' + sn);

		//text.setFont(NULL, NULL, NULL, NULL);   // 透明处理
		text.putText(frame, msg, cvPoint(point.x + 2, point.y - 5), Scalar(255, 255, 255));
		//printf("face_rect=[%d, %d, %d, %d], neighbors=%d\n", point.x, point.y, point.width, point.height, neighbors);
	
		// 框选住face
		cv::rectangle(frame, point, Scalar(230, 255, 0), 4);
	}
	else{
		cv::rectangle(frame, Rect(point.x - 2, point.y - 26, 110, 26), Scalar((int)g_rectrgb[predicted + 2], (int)g_rectrgb[predicted + 1], (int)g_rectrgb[predicted]), -1);

		text.putText(frame, g_peoplenames[predicted].c_str(), cvPoint(point.x + 2, point.y - 5), Scalar(255, 255, 255));
	
		// 框选住face
		cv::rectangle(frame, point, Scalar((int)g_rectrgb[predicted + 2], (int)g_rectrgb[predicted + 1], (int)g_rectrgb[predicted]), 4);
	}
}

std::vector<Rect> trackFaces(std::vector<Rect> newpoints)
{
	std::vector<Rect> outpoint;
	Rect point;
	float deta[DETECT_FACES_MAX ^ 2] = { 0 }; // 每张face分别对应上一时刻的faces的距离
	POINT_SN detasn;						   // NOTE: 每张face分别对应上一时刻的faces的距离的所有组序号
	float deta_ex[DETECT_FACES_MAX] = { 0 };   // 每张face与上一时刻的faces的最近的一个距离
	POINT_SN_EX detasn_ex;					   // NOTE: 每张face与上一时刻的faces的最近距离的一组序号
	int deta_x = 0;
	int deta_y = 0;
	int count = 0;

	if (fristruningflag){
		fristruningflag = false;
		lastdetectpoints.assign(newpoints.begin(), newpoints.begin() + newpoints.size());
		outpoint.assign(lastdetectpoints.begin(), lastdetectpoints.begin() + lastdetectpoints.size());
	}
	else{
		/*for (int i = 0; i < newpoints.size(); i++){
		fprintf(newpointfile, "%03d: %d\t %d\n", i, newpoints[i].x, newpoints[i].y);
		fflush(newpointfile);
		}
		for (int i = 0; i < lastdetectpoints.size(); i++){
		fprintf(lastpointfile, "%03d: %d\t %d\n", i, lastdetectpoints[i].x, lastdetectpoints[i].y);
		fflush(lastpointfile);
		}*/

		// 求得 当前每检测到的一张face 分别与 上一刻检测到的每一张face 的距离 以及一组序号
		for (int i = 0; i < newpoints.size(); i++){
			for (int j = 0; j < lastdetectpoints.size(); j++){
				deta_x = abs(newpoints[i].x - lastdetectpoints[j].x);
				deta_y = abs(newpoints[i].y - lastdetectpoints[j].y);

				detasn.newpointsn[j + i * lastdetectpoints.size()] = i;
				detasn.lastpointsn[j + i * lastdetectpoints.size()] = j;
				deta[j + i * lastdetectpoints.size()] = sqrt(deta_x ^ 2 + deta_y ^ 2);
			}
		}

		// 对 当前每检测到的一张face 分别与 上一刻检测到的每一张face 求得的距离进行升序排列
		for (int i = 0; i < newpoints.size(); i++){
			quickSort(deta + i * 3, 0, lastdetectpoints.size() - 1, detasn.newpointsn + i * 3, detasn.lastpointsn + i * 3);
		}

		// 对 当前检测到的每一张face 与 上一刻距离最近的一张face 的距离与标签做判别
		for (int i = 0; i < newpoints.size(); i++){
			deta_ex[i] = deta[i*lastdetectpoints.size()];
			detasn_ex.newpointsn[i] = detasn.newpointsn[i*lastdetectpoints.size()];
			detasn_ex.lastpointsn[i] = detasn.lastpointsn[i*lastdetectpoints.size()];
		}
		for (int i = 0; i < newpoints.size(); i++){
			quickSort_ex(detasn_ex.lastpointsn, 0, newpoints.size() - 1, detasn_ex.newpointsn, deta_ex);
		}

		// 对当前检测到的face，按照计算结果重新排序
		for (int i = 0; i < newpoints.size(); i++){
			outpoint.push_back(newpoints[detasn_ex.newpointsn[i]]);
		}

		lastdetectpoints.assign(newpoints.begin(), newpoints.begin() + newpoints.size());
	}

	return outpoint;
}

/*快速排序例子*/
//void quickSort(int s[], int l, int r)
//{
//	if (l< r)
//	{
//		int i = l, j = r, x = s[l];
//		while (i < j)
//		{
//			while (i < j && s[j] >= x) // 从右向左找第一个小于x的数  
//				j--;
//			if (i < j)
//				s[i++] = s[j];
//			while (i < j && s[i]< x) // 从左向右找第一个大于等于x的数  
//				i++;
//			if (i < j)
//				s[j--] = s[i];
//		}
//		s[i] = x;
//		quickSort(s, l, i - 1); // 递归调用  
//		quickSort(s, i + 1, r);
//	}
//}

void quickSort(float deta[], int start, int datanumber, int newpointsn[], int lastpointsn[])
{
	if (start < datanumber){
		int	  i = start, j = datanumber;
		float x = deta[start];
		int s_newpointsn  = newpointsn[start];
		int s_lastpointsn = lastpointsn[start];

		while (i < j){
			while (i < j && deta[j] >= x) // 从右向左找第一个小于x的数 
				j--;
			if (i < j){
				deta[i] = deta[j];
				newpointsn[i] = newpointsn[j];
				lastpointsn[i] = lastpointsn[j];
				i++;
			}
			
			while (i < j && deta[i] < x) // 从左向右找第一个大于等于x的数
				i++;
			if (i < j){
				deta[j] = deta[i];
				newpointsn[j] = newpointsn[i];
				lastpointsn[j] = lastpointsn[i];
				j--;
			}
		}
		deta[i] = x;
		newpointsn[i] = s_newpointsn;
		lastpointsn[i] = s_lastpointsn;

		quickSort(deta, start, i - 1, newpointsn, lastpointsn);
		quickSort(deta, i + 1, datanumber, newpointsn, lastpointsn);
	}
}

void quickSort_ex(int lastpointsn[], int start, int datanumber, int newpointsn[], float deta[])
{
	if (start < datanumber){
		int	  i = start, j = datanumber;
		float x = deta[start];
		int s_newpointsn = newpointsn[start];
		int s_lastpointsn = lastpointsn[start];

		while (i < j){
			while (i < j && lastpointsn[j] >= x) // 从右向左找第一个小于x的数 
				j--;
			if (i < j){
				deta[i] = deta[j];
				newpointsn[i] = newpointsn[j];
				lastpointsn[i] = lastpointsn[j];
				i++;
			}

			while (i < j && lastpointsn[i] < x) // 从左向右找第一个大于等于x的数
				i++;
			if (i < j){
				deta[j] = deta[i];
				newpointsn[j] = newpointsn[i];
				lastpointsn[j] = lastpointsn[i];
				j--;
			}
		}
		deta[i] = x;
		newpointsn[i] = s_newpointsn;
		lastpointsn[i] = s_lastpointsn;

		quickSort_ex(lastpointsn, start, i - 1, newpointsn, deta);
		quickSort_ex(lastpointsn, i + 1, datanumber, newpointsn, deta);
	}
}