
// FaceRecognitionApp.h : PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号
#include <opencv.hpp>

using namespace cv;

// CFaceRecognitionAppApp: 
// 有关此类的实现，请参阅 FaceRecognitionApp.cpp
//

class CFaceRecognitionAppApp : public CWinApp
{
public:
	CFaceRecognitionAppApp();

	static UINT OpenAndGetCameraThreadProc(LPVOID lpParameter);

// 重写
public:
	virtual BOOL InitInstance();

	int ExcitingMaxAreaFunction();
	void CaptureAndDisplay(int maxareasn, bool addfacesflag = false, bool concealedshootingflag = false);
	CRect GetMasterFramePoint();
	void TrainAndStorage(int maxareasn = -1, bool addfacesflag = false);
	int Mat2CImage(Mat *mat, CImage &img);

public:
	int  snapnumber_;
	CDC* ptheappdlgdc_;
	CString personname_;

// 实现
	DECLARE_MESSAGE_MAP()
};

extern CFaceRecognitionAppApp theApp;